function main()
{
	var myDiv = document.getElementById('myDiv1');
	
	writeOnPanel('NodeName:' + myDiv.nodeName);
	writeOnPanel('NodeValue: ' + myDiv.nodeValue);
	writeOnPanel('NodeType: ' + myDiv.nodeType);
	
	writeOnPanel('Node.ELEMENT_NODE = ' + Node.ELEMENT_NODE)
	writeOnPanel('Node.ATTRIBUTE_NODE = ' + Node.ATTRIBUTE_NODE)
	writeOnPanel('Node.TEXT_NODE  = ' + Node.TEXT_NODE )
	writeOnPanel('Node.COMMENT_NODE  = ' + Node.COMMENT_NODE  )
	writeOnPanel('Node.CDATA_SECTION_NODE  = ' + Node.CDATA_SECTION_NODE )
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel1');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
function main()
{
	var html = document.documentElement;
	
	var head = html.firstChild;
	var body = html.lastChild;	
	
	writeOnPanel(head.nodeName);
	var headChilds = head.childNodes;
	for(var i = 0; i < headChilds.length; i++)
	{
		var headChild = headChilds[i];
		writeOnPanel(headChild.nodeName);
		
		if(headChild.nodeType === Node.COMMENT_NODE)
		{
			writeOnPanel(headChild.nodeValue);		
		}
	}	
	
	writeOnPanel(body.nodeName);	
	var bodyChilds = body.childNodes;
	for(var i = 0; i < bodyChilds.length; i++)
	{
		var bodyChild = bodyChilds[i];
		writeOnPanel(bodyChild.nodeName);
		
		if(bodyChild.nodeType === Node.COMMENT_NODE)
		{
			writeOnPanel(bodyChild.nodeValue);		
			
			 console.info(bodyChild);
        
			var guillermitos = bodyChild.childNodes;
			for(var j = 0; j < guillermitos; j++)
			{
				var guillermito = guillermitos[j];
				console.info(j+ ':' + guillermito.nodeName);
			}
		}
		
		if(bodyChild.hasChildNodes())
		{
			writeOnPanel(bodyChild.childNodes.length);	
		}
	}
}

function button_click()
{
	var body = document.documentElement.childNodes[document.documentElement.childNodes.length - 1]; // document.documentElement.lastChild;
	var bodyChilds = body.childNodes;
	
	var div1 = bodyChilds[0].nextSibling.nextSibling.nextSibling;
	div1.setAttribute('class', 'red');
	
	var h1 = div1.previousSibling.previousSibling;
	h1.setAttribute('class', 'blue');
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel1');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

//main();
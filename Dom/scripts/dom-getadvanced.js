function main()
{
	var divs = document.getElementsByTagName('div');
	
	var divsLength = divs.length;
	writeOnPanel('Divs length: ' + divsLength);
	
	for(var i = 0; i < divsLength; i++)
	{
		var currentDiv = divs[i];
		var currentDivAttrs = currentDiv.attributes;
		
		writeOnPanel('****** Div[' + i + '] ******');
		for(var j = 0; j < currentDivAttrs.length; j++)
		{
			var currentAttr = currentDivAttrs.item(j);
			writeOnPanel(currentAttr.nodeName + ' = ' + currentAttr.nodeValue);
		}
	}	
	
	var psNumberOnDiv1 = divs[0].getElementsByTagName('p');
	writeOnPanel('Paragraphs on div1 :' + psNumberOnDiv1.length);	
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel1');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
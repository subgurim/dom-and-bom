function main()
{
	var panel = document.getElementById('panel1');

	var text1 = document.createTextNode('text 1');
	var p1 = document.createElement('p');
	p1.setAttribute('id', 'p1');
	p1.appendChild(text1);	
	panel.appendChild(p1);
	
	var text2 = document.createTextNode('text 2');
	var a2 = document.createElement('a');
	/*var a2Attr = document.createAttribute('href');
	a2Attr.nodeValue="http://google.com";
	a2.attributes.setNamedItem(a2Attr);*/
	a2.setAttribute('href', 'http://google.com');
	a2.appendChild(text2);
	var p2 = document.createElement('p');
	p2.setAttribute('id', 'p2');
	p2.appendChild(a2);
	panel.insertBefore(p2, p1);
}

function button_click()
{		
	var panel = document.getElementById('panel1');

	var p1 = document.getElementById('p1');
	panel.removeChild(p1);
	
	var text3 = document.createTextNode('text 3');
	var p3 = document.createElement('p');
	p3.setAttribute('id', 'p3');
	p3.appendChild(text3);
	var p2 = panel.getElementsByTagName('p')[0];	
	panel.replaceChild(p3, p2);
}



main();
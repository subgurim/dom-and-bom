function main()
{
	var div1 = document.getElementById('myDiv1');
	var p  = div1.getElementsByTagName('p')[0];
	var a = p.getElementsByTagName('a')[0];
	
	a.href = 'http://google.com';
	a.target = '_blank';
	
	a.style.color = 'green';
	a.style.fontWeight = 'bold';
	writeOnPanel(a.style.color);
	
	var h1 = document.getElementsByTagName('h1')[0];
	h1.className = 'red';
		
	writeOnPanel(h1.style.backgroundColor);
	writeOnPanel(getStyle(h1, 'background-color'));

}

function getStyle(elemento, propiedadCss) {
  var valor = "";
  if(document.defaultView && document.defaultView.getComputedStyle){
	// No IE
    valor = document.defaultView.getComputedStyle(elemento, '').getPropertyValue(propiedadCss);
  }
  else if(elemento.currentStyle) {
	// IE
    propiedadCss = propiedadCss.replace(/\-(\w)/g, function (strMatch, p1) {
      return p1.toUpperCase();
    });
    valor = elemento.currentStyle[propiedadCss];
  }
  return valor;
}

function button_click()
{
	var html = '<a href="http://es.lipsum.com/">Lorem ipsum</a> dolor sit amet, consectetur adipiscing elit. Vestibulum ac elit eros. Duis nec dui aliquet, porttitor neque ut, congue quam. Morbi vehicula lacus ac enim congue pretium.';
	
	var p_div2 = document.getElementById('myDiv2').firstChild.nextSibling;
	p_div2.innerHTML = html;
	
	var p_div1 = document.getElementById('myDiv1').firstChild.nextSibling;
	p_div1.innerText = html;
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel1');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
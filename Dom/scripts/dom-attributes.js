function main()
{
	var div2 = document.getElementById('myDiv2');
	
	var classAtributeNode = div2.attributes.getNamedItem('class');
	writeOnPanel('ClassName: ' + classAtributeNode.nodeName);
	writeOnPanel('ClassName: ' + classAtributeNode.nodeType);
	writeOnPanel('ClassName: ' + classAtributeNode.nodeValue);

	var className = div2.getAttribute('class'); //attributes.getNamedItem('class').
	writeOnPanel('ClassName: ' + className);
	
	var dataTwoPlusTwo = div2.getAttribute('data-twoplustwo'); //  //attributes.getNamedItem('data-twoplustwo').
	writeOnPanel('dataTwoPlusTwo: ' + dataTwoPlusTwo);
	
	
	div2.setAttribute('data-fourplusfour', 16); //attributes.setNamedItem(data-fourplusfour).value = 16.	
	var dataFourPlusFour = div2.getAttribute('data-fourplusfour'); //  //attributes.getNamedItem('data-fourplusfour').
	writeOnPanel('dataFourPlusFour: ' dataFourPlusFour);
	
	
	div2.removeAttribute('data-twoplustwo'); //div2.attributes.removeNamedItem('data-twoplustwo');
	writeOnPanel('Deleted twoplustwo:' + div2.getAttribute('data-twoplustwo'));
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel1');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
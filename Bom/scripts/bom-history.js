function main()
{
	writeOnPanel('longitud del historial: ' + history.length);
	
	var select = document.getElementById('historyGoBackSelector');
	for(var i = 0; i<history.length; i++)
	{
		var option = document.createElement('option');
		option.value = -1-i;
		option.text = -1-i;
		select.appendChild(option);
	}
}

window.goBackN = function()
{
	var select = document.getElementById('historyGoBackSelector');
	var selectedIndex = select.selectedIndex;
	var selectedOption = select.options[selectedIndex];
	var n = selectedOption.value;
	
	history.go(n);
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
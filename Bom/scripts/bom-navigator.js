function main()
{
	writeOnPanel('appCodeName: ' + navigator.appCodeName);
	writeOnPanel('appName: ' + navigator.appName);
	writeOnPanel('appVersion: ' + navigator.appVersion);
	writeOnPanel('cookieEnable: ' + navigator.cookieEnabled);
	writeOnPanel('javaEnabled(): ' + navigator.javaEnabled());
	writeOnPanel('platform: ' + navigator.platform);
	writeOnPanel('userAgent: ' + navigator.userAgent);
	writeOnPanel('plugins.length: ' + navigator.plugins.length);
	
	for (var i = 0; i < navigator.plugins.length; i++)
	{
		var plugin = navigator.plugins[i];
		writeOnPanel('plugin['+i+']: ' + plugin.name);
	}
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
function main()
{
	writeOnPanel('lastModified: ' + document.lastModified);
	writeOnPanel('referrer: ' + document.referrer);
	writeOnPanel('title: ' + document.title);
	writeOnPanel('URL: ' + document.URL);	
	
	writeOnPanel('Forms count: ' + document.forms.length);	

	writeOnPanel('Links count: ' + document.links.length);	
	for (var i = 0; i < document.links.length; i++)
	{
		var link = document.links[i];
		writeOnPanel('Link['+i+'] href: ' + link.href);	
		writeOnPanel('Link['+i+'] text: ' + link.innerHTML);	
	}
}

window.openWindow = function(){
	var what2Write = document.getElementById('what2Write').value;
	
	var newWindow = window.open('about:blank', 'mywindow', 'width=300,height=250');
	newWindow.document.open();
	newWindow.document.write('<html><head><title>mywindow</title></head>');
	newWindow.document.write('<body><h1>'+what2Write+'</h1></body></html>');
	newWindow.document.close();	
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
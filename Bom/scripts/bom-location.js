function main()
{
	writeOnPanel('hash' + location.hash);
	writeOnPanel('host' + location.host);
	writeOnPanel('hostname' + location.hostname);
	writeOnPanel('href' + location.href);
	writeOnPanel('pathname' + location.pathname);
	writeOnPanel('port' + location.port);
	writeOnPanel('protocol' + location.protocol);
	writeOnPanel('search' + location.search);
}

window.reloadPage = function()
{
	location.reload(true);
}

window.goTo = function()
{
	var url = document.getElementById('url').value;
	location.href = url; //location.assign(url); location.replace(url);
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();
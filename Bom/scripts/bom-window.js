window.tellMeThePosition = function(){
	var windowXPosition = window.screenLeft != undefined ? window.screenLeft : window.screenX; // IE window.screenLeft
	var windowYPosition = window.screenTop != undefined ? window.screenTop : window.screenY; // IE window.screenTop
	
	writeOnPanel('Desde la esquina superior derecha, la ventana está a '+windowXPosition+' píxeles horizontales y '+windowYPosition+' píxeles verticales');

	return false;
}

window.tellMeTheSize = function(){
	var intervalCount = 0;
	var loadingInterval = window.setInterval(function(){
		npoints = intervalCount% 4;
		pointsStr = new Array(npoints+1).join('.');
		writeOnPanel('loading' + pointsStr);
		
		intervalCount++;
	} , 250);

	var waitFor = parseInt(document.getElementById('waitForXSecconds').value) * 1000;
	window.setTimeout(function(){
		window.clearInterval(loadingInterval);
		calculateAndWriteSize();
		
		}
		, waitFor);
}

window.openWindow = function()
{
	alert('Iniciamos el proceso para abrir una nueva ventana');
	if(confirm('Seguro que quieres que abramos una nueva ventana?'))
	{
		var newWindowToOpenUrl = document.getElementById('newWindowUrl').value;
		
		if(!newWindowToOpenUrl)
		{
			newWindowToOpenUrl = prompt('No has indicado ninguna url a la que ir, por favor indícala ahora');
		}
		var newWindow = window.open(newWindowToOpenUrl,"Google","width=300,height=250"); //window.open(URL,name,specs,replace) -->  http://www.w3schools.com/jsref/met_win_open.asp
		
		var interval = window.setInterval(function(){
			var randomX = Math.floor((Math.random() * 300) + -150);;
			var randomY = Math.floor((Math.random() * 300) + -150);;
			newWindow.moveBy(randomX,randomY);
		}, 100);
		
		window.setTimeout(function(){
			window.clearInterval(interval);
			newWindow.close();
		}, 5000);
	}
}

function calculateAndWriteSize()
{
	var windowXWidth = window.innerWidth; //outerWidth  //document.body.offsetWidth
	var windowYHeight = window.innerHeight; //outerHeight //document.body.offsetHeight
	writeOnPanel('La ventana mide '+windowXWidth+' de ancho y '+windowYHeight+' de alto');
	
	return false;
}


function writeOnPanel(text)
{
	document.getElementById('panel').innerText = text;
}
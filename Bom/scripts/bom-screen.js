function main()
{
	writeOnPanel('availHeight: ' + screen.availHeight);
	writeOnPanel('availWidth: ' + screen.availWidth);
	writeOnPanel('colorDepth: ' + screen.colorDepth);
	writeOnPanel('height: ' + screen.height);
	writeOnPanel('width: ' + screen.width);
}

function writeOnPanel(txt)
{
	var panel = document.getElementById('panel');
	
	var text = document.createTextNode(txt);
	var p = document.createElement('p');
	p.appendChild(text);
	
	panel.appendChild(p);
}

main();